export type Cinema = {
  maHeThongRap: string;
  tenHeThongRap: string;
  biDanh: string;
  logo: string;
};
export type MovieCinema = {
  maRap: number;
  tenRap: string;
};
export type Cineplexs = {
  maCumRap: string;
  tenCumRap: string;
  diaChi: string;
  danhSachRap: MovieCinema[];
};
