import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import "./index.css";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { StyleProvider } from "@ant-design/cssinjs";
import { store } from "store";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <BrowserRouter>
    <Provider store={store}>
      <ToastContainer autoClose={1000} />
      <StyleProvider hashPriority="high">
        <App />
      </StyleProvider>
    </Provider>
  </BrowserRouter>
);
