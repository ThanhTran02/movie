import { apiInstance } from "constant";
import { Movie, MovieBaner } from "types";

const api = apiInstance({
  baseURL: import.meta.env.VITE_QUAN_LY_PHIM_API,
});
export const quanLyPhimService = {
  getMovieBanner: () =>
    api.get<ApiResponse<MovieBaner[]>>("/LayDanhSachBanner"),
  getMovieList: (query: string) =>
    api.get<ApiResponse<any>>(`/LayDanhSachPhimPhanTrang${query}`),
  getMovieDetail: (query: string) =>
    api.get<ApiResponse<any>>(`/LayThongTinPhim${query}`),

  // admin
  getMovieListAdmin: () =>
    api.get<ApiResponse<Movie[]>>("/LayDanhSachPhim?maNhom=GP00"),
  getSearchMovieListAdmin: (tenPhim: string) =>
    api.get<ApiResponse<Movie[]>>(
      `/LayDanhSachPhim?maNhom=GP00&tenPhim=${tenPhim}`
    ),
  deleteMovieAdmin: (query: string) =>
    api.delete<ApiResponse<any>>(`/XoaPhim${query}`),
  editMovieAdmin: (formData: FormData) =>
    api.post<ApiResponse<any>>(`/CapNhatPhimUpload`, formData),
  addMovieAdmin: (formData: FormData) =>
    api.post<ApiResponse<any>>(`/ThemPhimUploadHinh`, formData),
};
