import { apiInstance } from "constant";
import { Cinema, Cineplexs } from "types";

const api = apiInstance({
  baseURL: import.meta.env.VITE_QUAN_LY_RAP_API,
});
export const quanLyRapService = {
  getCinema: () => api.get<ApiResponse<Cinema[]>>("/LayThongTinHeThongRap"),
  getCineplex: (query: string) =>
    api.get<ApiResponse<Cineplexs[]>>(`LayThongTinCumRapTheoHeThong${query}`),
  getShowtimesCineplex: (query: string) =>
    api.get<ApiResponse<any[]>>(`LayThongTinLichChieuHeThongRap${query}`),
  getInfoLichChieuPhim: (query: string) =>
    api.get<ApiResponse<any>>(`LayThongTinLichChieuPhim${query}`),
};
