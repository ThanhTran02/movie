import { apiInstance } from "constant";
import { AccountSchemaType, LoginSchemaType, RegisterSchemaType } from "schema";
import { User, UserInfo } from "types";

const api = apiInstance({
  baseURL: import.meta.env.VITE_QUAN_LY_NGUOI_DUNG_API,
});
export const quanLyNguoiDungServices = {
  login: (payload: LoginSchemaType) =>
    api.post<ApiResponse<User>>("/dangNhap", payload),
  register: (payload: RegisterSchemaType) => api.post("dangky", payload),
  getUser: () => api.post<ApiResponse<UserInfo>>("/ThongTinTaiKhoan"),
  updateUser: (payload: AccountSchemaType) =>
    api.put("/CapNhatThongTinNguoiDung", payload),

  // admin
  getUserListAdmin: () =>
    api.get<ApiResponse<any[]>>("/LayDanhSachNguoiDung?MaNhom=GP03"),
  getSearchUserListAdmin: (tuKhoa: string) =>
    api.get<ApiResponse<any[]>>(
      `/LayDanhSachNguoiDung?MaNhom=GP03&tuKhoa=${tuKhoa}`
    ),
  deleteUserAdmin: (query: string) =>
    api.delete<ApiResponse<any>>(`/XoaNguoiDung${query}`),
  updateUserAdmin: (payload: AccountSchemaType) =>
    api.post("/CapNhatThongTinNguoiDung", payload),
  addUserAdmin: (payload: AccountSchemaType) =>
    api.post("/ThemNguoiDung", payload),
};
