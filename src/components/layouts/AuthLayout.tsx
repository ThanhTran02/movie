import { Footer } from "components/ui";
import Navbar from "components/ui/Navbar";
import { useAuth } from "hooks";
import { Navigate, Outlet } from "react-router-dom";

export const AuthLayout = () => {
  const { user } = useAuth();
  if (user) {
    return <Navigate to="/" />;
  }
  return (
    <div className=" flex flex-col bg-bg  relative overflow-x-hidden ">
      <div className="fixed top-0 left-0 w-full z-50 bg-white border-b-gray-200 border-b-2   ">
        <Navbar />
      </div>
      <div className="flex-1  items-center flex justify-center p-20 bg-cover object-contain ">
        <div className="absolute top-0 left-0 w-full h-full bg-black z-10 opacity-50 "></div>
        <div className=" w-[400px] p-[30px] pt-2 mt-20 bg-white z-20  rounded-md">
          <Outlet />
        </div>
      </div>
      <div className="w-screen bg-neutral-200">
        <Footer />
      </div>
    </div>
  );
};

export default AuthLayout;
