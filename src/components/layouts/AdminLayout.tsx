import { Sibar } from "components/ui";
import { Outlet } from "react-router-dom";

export const AdminLayout = () => {
  return (
    <div>
      <Sibar />
      <div>
        <Outlet />
      </div>
    </div>
  );
};

export default AdminLayout;
