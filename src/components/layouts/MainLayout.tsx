import { Footer, Navbar } from "components/ui";
import { Outlet } from "react-router-dom";
import { styled } from "styled-components";

export const MainLayout = () => {
  return (
    <div>
      <Container className="  relative ">
        <div className="fixed top-0 left-0 w-full  z-50 border-b-2 bg-white border-b-gray-200 ">
          <Navbar />
        </div>
        <div className="pt-[60px] items-center flex justify-center   bg-cover object-contain ">
          <Outlet />
        </div>
      </Container>
      <div className="bg-neutral-200">
        <Footer />
      </div>
    </div>
  );
};

export default MainLayout;
const Container = styled.div`
  width: var(--max-width);
  margin: auto;
`;
