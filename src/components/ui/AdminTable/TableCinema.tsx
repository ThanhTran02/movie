import { RootState, useAppDispatch } from "store";
import { useEffect } from "react";
import { AiFillEdit, AiOutlineDelete } from "react-icons/ai";
import { useSelector } from "react-redux";
import { Cinema } from "types";
import { getCinemaListThunk } from "store/quanLyRap/thunk";
export const TableCinema = () => {
  const { cinemaList } = useSelector((state: RootState) => state.quanLiRap);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getCinemaListThunk());
  }, []);

  return (
    <div className="relative overflow-x-auto shadow-md sm:rounded-lg w-full mt-5 ">
      <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
          <tr>
            <th scope="col" className="px-6 py-3">
              <div className="flex items-center">
                <p>Mã Hệ thống rạp</p>
                <svg
                  className="w-3 h-3 ml-1.5"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="currentColor"
                  viewBox="0 0 24 24"
                >
                  <path d="M8.574 11.024h6.852a2.075 2.075 0 0 0 1.847-1.086 1.9 1.9 0 0 0-.11-1.986L13.736 2.9a2.122 2.122 0 0 0-3.472 0L6.837 7.952a1.9 1.9 0 0 0-.11 1.986 2.074 2.074 0 0 0 1.847 1.086Zm6.852 1.952H8.574a2.072 2.072 0 0 0-1.847 1.087 1.9 1.9 0 0 0 .11 1.985l3.426 5.05a2.123 2.123 0 0 0 3.472 0l3.427-5.05a1.9 1.9 0 0 0 .11-1.985 2.074 2.074 0 0 0-1.846-1.087Z" />
                </svg>
              </div>
            </th>
            <th scope="col" className="px-6 py-3">
              Hình ảnh
            </th>
            <th scope="col" className="px-6 py-3">
              <div className="flex items-center">
                <p>Tên hệ thống rạp </p>
                <svg
                  className="w-3 h-3 ml-1.5"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="currentColor"
                  viewBox="0 0 24 24"
                >
                  <path d="M8.574 11.024h6.852a2.075 2.075 0 0 0 1.847-1.086 1.9 1.9 0 0 0-.11-1.986L13.736 2.9a2.122 2.122 0 0 0-3.472 0L6.837 7.952a1.9 1.9 0 0 0-.11 1.986 2.074 2.074 0 0 0 1.847 1.086Zm6.852 1.952H8.574a2.072 2.072 0 0 0-1.847 1.087 1.9 1.9 0 0 0 .11 1.985l3.426 5.05a2.123 2.123 0 0 0 3.472 0l3.427-5.05a1.9 1.9 0 0 0 .11-1.985 2.074 2.074 0 0 0-1.846-1.087Z" />
                </svg>
              </div>
            </th>

            <th scope="col" className="px-6 py-3">
              Bí danh
            </th>
            <th scope="col" className="px-6 py-3">
              Hành động
            </th>
          </tr>
        </thead>
        <tbody>
          {cinemaList?.map((cinema: Cinema) => (
            <tr
              key={cinema.maHeThongRap}
              className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600"
            >
              <th
                scope="row"
                className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
              >
                {cinema.maHeThongRap}
              </th>
              <td>
                <img
                  src={cinema.logo}
                  alt="movie"
                  className="object-contain h-12 w-12"
                />
              </td>
              <td>{cinema.tenHeThongRap}</td>
              <td>{cinema.biDanh}</td>
              <td className=" px-5">
                <button className="">
                  <AiFillEdit className="text-2xl cursor-not-allowed" />
                </button>
                <button className="ml-8">
                  <AiOutlineDelete className="text-2xl cursor-not-allowed" />
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default TableCinema;
