import { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { Modal, Switch } from "antd";
import { toast } from "react-toastify";
import { SubmitHandler, useForm } from "react-hook-form";
import { styled } from "styled-components";
import { zodResolver } from "@hookform/resolvers/zod";

import { RootState, useAppDispatch } from "store";
import { AiFillEdit, AiOutlineDelete } from "react-icons/ai";
import { Movie } from "types";
import {
  deleteMovieThunkAdmin,
  editMovieThunkAdmin,
  getMovieListAdminThunk,
} from "store/quanLyPhim/thunk";
import { MovieSchema, MovieSchemaType } from "schema";
import { Button, Input } from "..";

type NameValue = "sapChieu" | "hot" | "dangChieu";

export const TableFlims = () => {
  const { movieListAdmin } = useSelector(
    (state: RootState) => state.quanLiPhim
  );
  const dispatch = useAppDispatch();
  const [displayedProducts, setDisplayedProducts] = useState(true);

  const confirm = (maPhim: number) => {
    Modal.confirm({
      title: "Do you Want to delete these items?",
      icon: <ExclamationCircleOutlined />,
      onOk() {
        dispatch(deleteMovieThunkAdmin(maPhim))
          .unwrap()
          .then(() => {
            toast.success("Xoá thành công phim");
            setDisplayedProducts(!displayedProducts);
          });
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };
  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  // register
  let movieChoose = useRef<null | Movie>();
  const {
    reset,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<MovieSchemaType>({
    mode: "onChange",
    resolver: zodResolver(MovieSchema),
  });
  //switch

  const onChange = (name: NameValue) => {
    return (checked: boolean) => {
      if (movieChoose.current) {
        movieChoose.current[name] = checked;
        reset(movieChoose.current);
      }
    };

    // return (checked) => {
    // };
  };
  let formData = new FormData();
  const onSubmit: SubmitHandler<any> = (value) => {
    for (let key in value) {
      formData.append(key, value[key]);
    }

    dispatch(editMovieThunkAdmin(formData))
      .unwrap()
      .then(() => {
        toast.success("Cập nhật thông tin Phim Thành công");
        handleCancel();
        setDisplayedProducts(!displayedProducts);
      });
  };

  useEffect(() => {
    dispatch(getMovieListAdminThunk(""));
  }, [displayedProducts, reset]);

  return (
    <Container className="relative overflow-x-auto shadow-md sm:rounded-lg w-full mt-5 ">
      <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
          <tr>
            <th scope="col" className="px-6 py-3">
              <div className="flex items-center">
                <p>Mã phim</p>
                <svg
                  className="w-3 h-3 ml-1.5"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="currentColor"
                  viewBox="0 0 24 24"
                >
                  <path d="M8.574 11.024h6.852a2.075 2.075 0 0 0 1.847-1.086 1.9 1.9 0 0 0-.11-1.986L13.736 2.9a2.122 2.122 0 0 0-3.472 0L6.837 7.952a1.9 1.9 0 0 0-.11 1.986 2.074 2.074 0 0 0 1.847 1.086Zm6.852 1.952H8.574a2.072 2.072 0 0 0-1.847 1.087 1.9 1.9 0 0 0 .11 1.985l3.426 5.05a2.123 2.123 0 0 0 3.472 0l3.427-5.05a1.9 1.9 0 0 0 .11-1.985 2.074 2.074 0 0 0-1.846-1.087Z" />
                </svg>
              </div>
            </th>
            <th scope="col" className="px-6 py-3">
              Hình ảnh
            </th>
            <th scope="col" className="px-6 py-3">
              <div className="flex items-center">
                <p>Tên phim</p>
                <svg
                  className="w-3 h-3 ml-1.5"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="currentColor"
                  viewBox="0 0 24 24"
                >
                  <path d="M8.574 11.024h6.852a2.075 2.075 0 0 0 1.847-1.086 1.9 1.9 0 0 0-.11-1.986L13.736 2.9a2.122 2.122 0 0 0-3.472 0L6.837 7.952a1.9 1.9 0 0 0-.11 1.986 2.074 2.074 0 0 0 1.847 1.086Zm6.852 1.952H8.574a2.072 2.072 0 0 0-1.847 1.087 1.9 1.9 0 0 0 .11 1.985l3.426 5.05a2.123 2.123 0 0 0 3.472 0l3.427-5.05a1.9 1.9 0 0 0 .11-1.985 2.074 2.074 0 0 0-1.846-1.087Z" />
                </svg>
              </div>
            </th>

            <th scope="col" className="px-6 py-3">
              Mã nhóm
            </th>
            <th scope="col" className="px-6 py-3">
              Đánh giá
            </th>

            <th scope="col" className="px-6 py-3">
              Ngày khởi chiếu
            </th>

            <th scope="col" className="px-6 py-3">
              Hành động
            </th>
          </tr>
        </thead>
        <tbody>
          {movieListAdmin.map((movie: Movie) => (
            <tr
              key={movie.maPhim}
              className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600"
            >
              <th
                scope="row"
                className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
              >
                {movie.maPhim}
              </th>
              <td>
                <img
                  src={movie.hinhAnh}
                  alt="movie"
                  className="object-contain h-20 w-20"
                />
              </td>
              <td>{movie.tenPhim}</td>
              <td>{movie.maNhom}</td>
              <td>{movie.danhGia}</td>
              <td>{movie.ngayKhoiChieu}</td>
              <td className=" ">
                <button
                  className="pl-3"
                  onClick={() => {
                    showModal();
                    movieChoose.current = Object.assign({}, movie);
                    reset(movieChoose.current);
                  }}
                >
                  <AiFillEdit className="text-2xl hover:text-white" />
                </button>

                <button className="ml-5" onClick={() => confirm(movie.maPhim)}>
                  <AiOutlineDelete className="text-2xl hover:text-white" />
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <Modal
        maskStyle={{ background: `rgba(0, 0, 0, 0.2)` }}
        width={1000}
        open={isModalOpen}
        footer
        onCancel={handleCancel}
      >
        <form className="px-10" onSubmit={handleSubmit(onSubmit)}>
          <InputS
            label="Mã Phim:"
            name="maPhim"
            register={register}
            error={errors?.maPhim?.message}
          />
          <InputS
            label="Tên Phim:"
            name="tenPhim"
            register={register}
            error={errors?.tenPhim?.message}
          />
          <InputS
            label="Hình ảnh:"
            name="hinhAnh"
            register={register}
            error={errors?.hinhAnh?.message}
          />
          <InputS
            label="Mô tả:"
            name="moTa"
            register={register}
            error={errors?.moTa?.message}
          />
          <InputS label="Mã nhóm:" name="maNhom" register={register} />
          <InputS
            label="Ngày khởi chiếu:"
            name="ngayKhoiChieu"
            register={register}
            error={errors?.ngayKhoiChieu?.message}
          />
          <div>
            <p className="my-2 text-lg font-semibold text-start text-slate-700">
              Hot:
            </p>
            <Switch
              checked={movieChoose.current?.hot}
              onChange={onChange("hot")}
            />
          </div>
          <div>
            <p className="my-2 text-lg font-semibold text-start text-slate-700">
              Đang chiếu:
            </p>
            <Switch
              checked={movieChoose.current?.dangChieu}
              onChange={onChange("dangChieu")}
            />
          </div>
          <div>
            <p className="my-2 text-lg font-semibold text-start text-slate-700">
              Sắp chiếu:
            </p>
            <Switch
              checked={movieChoose.current?.sapChieu}
              onChange={onChange("sapChieu")}
            />
          </div>
          <div className="text-right ">
            <Button
              htmlType="submit"
              className="mt-[20px] mb-[20px] w-[200px] !h-[50px] !bg-gradient-to-r !from-teal-400 !via-teal-500 !to-teal-600 hover:bg-gradient-to-br !font-bold !text-white !text-[16px]"
            >
              Lưu thay đổi
            </Button>
          </div>
        </form>
      </Modal>
    </Container>
  );
};

export default TableFlims;

const Container = styled.div``;
const InputS = styled(Input)`
  /* margin-top: 20px; */
  input {
    background-color: transparent !important;
    border: 1px solid black;
    color: black;
  }
`;
