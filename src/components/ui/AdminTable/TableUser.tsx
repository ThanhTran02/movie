import { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { Modal } from "antd";
import { toast } from "react-toastify";
import { styled } from "styled-components";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";

import { RootState, useAppDispatch } from "store";
import { AiFillEdit, AiOutlineDelete } from "react-icons/ai";
import {
  deleteUserThunkAdmin,
  getUserListThunkAdmin,
  updateUserAdminThunk,
} from "store/quanLyNguoiDung/thunk";
import { AccountSchema, AccountSchemaType } from "schema";
import { Button, Input } from "..";

export const TableUser = () => {
  const { userList } = useSelector((state: RootState) => state.quanLyNguoiDung);
  const [displayedProducts, setDisplayedProducts] = useState(true);

  const dispatch = useAppDispatch();
  const [isModalOpen, setIsModalOpen] = useState(false);

  const formRef = useRef(null);

  const confirm = (taiKhoan: string) => {
    Modal.confirm({
      title: "Do you Want to delete these items?",
      icon: <ExclamationCircleOutlined />,
      onOk() {
        dispatch(deleteUserThunkAdmin(taiKhoan))
          .unwrap()
          .then(() => {
            toast.success("Xoá thành công người dùng");
            setDisplayedProducts(!displayedProducts);
          });
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  let userChoose = useRef<null | any>();
  const { reset, register } = useForm<AccountSchemaType>({
    mode: "onSubmit",
    resolver: zodResolver(AccountSchema),
  });
  interface FormValues {
    [key: string]: string;
  }

  const onSubmit = () => {
    if (formRef.current) {
      const formData = new FormData(formRef.current);
      const values: FormValues = {};

      formData.forEach((value, name) => {
        values[name] = value.toString();
      });

      dispatch(updateUserAdminThunk(values))
        .unwrap()
        .then(() => {
          toast.success("Cập nhật thông tin người dùng Thành công");
          handleCancel();
          setDisplayedProducts(!displayedProducts);
        });
    }
  };

  useEffect(() => {
    dispatch(getUserListThunkAdmin(""));
  }, [displayedProducts]);
  return (
    <div className="relative overflow-x-auto shadow-md sm:rounded-lg w-full mt-5">
      <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
          <tr>
            <th scope="col" className="px-6 py-3">
              <div className="flex items-center">
                <p>Tài khoản</p>
                <svg
                  className="w-3 h-3 ml-1.5"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="currentColor"
                  viewBox="0 0 24 24"
                >
                  <path d="M8.574 11.024h6.852a2.075 2.075 0 0 0 1.847-1.086 1.9 1.9 0 0 0-.11-1.986L13.736 2.9a2.122 2.122 0 0 0-3.472 0L6.837 7.952a1.9 1.9 0 0 0-.11 1.986 2.074 2.074 0 0 0 1.847 1.086Zm6.852 1.952H8.574a2.072 2.072 0 0 0-1.847 1.087 1.9 1.9 0 0 0 .11 1.985l3.426 5.05a2.123 2.123 0 0 0 3.472 0l3.427-5.05a1.9 1.9 0 0 0 .11-1.985 2.074 2.074 0 0 0-1.846-1.087Z" />
                </svg>
              </div>
            </th>
            <th scope="col" className="px-6 py-3">
              Mật khẩu
            </th>
            <th scope="col" className="px-6 py-3">
              <div className="flex items-center">
                <p>Email</p>
                <svg
                  className="w-3 h-3 ml-1.5"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="currentColor"
                  viewBox="0 0 24 24"
                >
                  <path d="M8.574 11.024h6.852a2.075 2.075 0 0 0 1.847-1.086 1.9 1.9 0 0 0-.11-1.986L13.736 2.9a2.122 2.122 0 0 0-3.472 0L6.837 7.952a1.9 1.9 0 0 0-.11 1.986 2.074 2.074 0 0 0 1.847 1.086Zm6.852 1.952H8.574a2.072 2.072 0 0 0-1.847 1.087 1.9 1.9 0 0 0 .11 1.985l3.426 5.05a2.123 2.123 0 0 0 3.472 0l3.427-5.05a1.9 1.9 0 0 0 .11-1.985 2.074 2.074 0 0 0-1.846-1.087Z" />
                </svg>
              </div>
            </th>
            <th scope="col" className="px-6 py-3">
              Số điện thoại
            </th>

            <th scope="col" className="px-6 py-3">
              Họ tên
            </th>
            <th scope="col" className="px-6 py-3">
              Loại người dùng
            </th>
            <th scope="col" className="px-6 py-3">
              Hành động
            </th>
          </tr>
        </thead>
        <tbody>
          {userList.map((user: any, index: number) => (
            <tr
              className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600"
              key={index}
            >
              <th
                scope="row"
                className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
              >
                {user.taiKhoan}
              </th>
              <td>{user.matKhau}</td>
              <td>{user.email}</td>
              <td>{user.soDT}</td>
              <td>{user.hoTen}</td>
              <td>{user.maLoaiNguoiDung}</td>
              <td className=" items-center">
                <button
                  className=""
                  onClick={() => {
                    showModal();
                    userChoose.current = Object.assign({}, user, {
                      maNhom: "GP03",
                    });
                    reset(userChoose.current);
                  }}
                >
                  <AiFillEdit className="text-2xl hover:text-white" />
                </button>
                <button className="ml-8" onClick={() => confirm(user.taiKhoan)}>
                  <AiOutlineDelete className="text-2xl hover:text-white" />
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <Modal
        maskStyle={{ background: `rgba(0, 0, 0, 0.2)` }}
        width={1000}
        open={isModalOpen}
        footer
        onCancel={handleCancel}
      >
        <form
          className="px-10"
          onSubmit={(e) => {
            e.preventDefault();
            onSubmit();
          }}
          ref={formRef}
        >
          <InputS label="Tài khoản" name="taiKhoan" register={register} />
          <InputS label="Mật khẩu" name="matKhau" register={register} />
          <InputS label="Họ và tên" name="hoTen" register={register} />
          <InputS label="Số điện thoại" name="soDT" register={register} />
          <InputS label="Email" name="email" register={register} />
          <InputS label="Mã nhóm" name="maNhom" register={register} />
          <InputS
            label="Mã Người dùng"
            name="maLoaiNguoiDung"
            register={register}
          />
          <div className="text-right ">
            <Button
              htmlType="submit"
              className="mt-[20px] mb-[20px] w-[200px] !h-[50px] !bg-gradient-to-r !from-teal-400 !via-teal-500 !to-teal-600 hover:bg-gradient-to-br !font-bold !text-white !text-[16px]"
            >
              Lưu thay đổi
            </Button>
          </div>
        </form>
      </Modal>
    </div>
  );
};

export default TableUser;
const InputS = styled(Input)`
  /* margin-top: 20px; */
  input {
    background-color: transparent !important;
    border: 1px solid black;
    color: black;
  }
`;
