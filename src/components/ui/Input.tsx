import { HTMLInputTypeAttribute } from "react";
import { UseFormRegister } from "react-hook-form";
type InputProps = {
  register?: UseFormRegister<any>;
  error?: string;
  name?: string;
  placeholder?: string;
  type?: HTMLInputTypeAttribute;
  className?: string;
  disabled?: boolean;
  label?: string;
  value?: string;
};

export const Input = ({
  type,
  name,
  placeholder,
  error,
  register,
  className,
  disabled,
  label,
  value,
}: InputProps) => {
  return (
    <div className={`relative ${className} items-start`}>
      {label && (
        <p className="mb-4 text-lg font-semibold text-start text-slate-700">
          {label}
        </p>
      )}
      <input
        value={value}
        type={type}
        disabled={disabled}
        placeholder={placeholder}
        className="outline-none  block w-full p-3 text-gray-900 placeholder-red-600 border border-red-600 rounded-lg bg-gray-50 "
        {...register?.(name || "")}
      />

      {error && <p className="text-red-500 mt-2">{error}</p>}
    </div>
  );
};

export default Input;
