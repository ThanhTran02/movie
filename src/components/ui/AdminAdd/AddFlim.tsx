import { useRef } from "react";
import { toast } from "react-toastify";
import { styled } from "styled-components";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { AccountSchema, MovieSchemaType } from "schema";

import { Button, Input } from "..";
import { addMovieThunkAdmin } from "store/quanLyPhim/thunk";
import { useAppDispatch } from "store";
export const AddFlim = () => {
  const formRef = useRef(null);
  const dispatch = useAppDispatch();
  const {
    register,
    formState: { errors },
  } = useForm<MovieSchemaType>({
    mode: "onSubmit",

    resolver: zodResolver(AccountSchema),
  });

  const onSubmit = () => {
    if (formRef.current) {
      const formData = new FormData(formRef.current);

      const values: any = {};

      formData.forEach((value, name) => {
        values[name] = value.toString();
      });
      dispatch(addMovieThunkAdmin(values))
        .unwrap()
        .then(() => {
          toast.success("Thêm phim Thành công");
        });
    }
  };
  return (
    <form
      className="px-10"
      onSubmit={(e) => {
        e.preventDefault();
        onSubmit();
      }}
      ref={formRef}
    >
      <InputS
        label="Mã Phim:"
        name="maPhim"
        register={register}
        error={errors?.maPhim?.message}
      />
      <InputS
        label="Tên Phim:"
        name="tenPhim"
        register={register}
        error={errors?.tenPhim?.message}
      />
      <InputS
        label="Hình ảnh:"
        name="hinhAnh"
        register={register}
        error={errors?.hinhAnh?.message}
      />
      <InputS
        label="Mô tả:"
        name="moTa"
        register={register}
        error={errors?.moTa?.message}
      />
      <InputS label="Mã nhóm:" name="maNhom" register={register} />
      <InputS
        label="Ngày khởi chiếu:"
        name="ngayKhoiChieu"
        register={register}
        error={errors?.ngayKhoiChieu?.message}
      />
      <div>
        <p className="my-2 text-lg font-semibold text-start text-slate-700">
          Hot:
        </p>
      </div>
      <div>
        <p className="my-2 text-lg font-semibold text-start text-slate-700">
          Đang chiếu:
        </p>
      </div>
      <div>
        <p className="my-2 text-lg font-semibold text-start text-slate-700">
          Sắp chiếu:
        </p>
      </div>
      <div className="text-right ">
        <Button
          htmlType="submit"
          className="mt-[20px] mb-[20px] w-[200px] !h-[50px] !bg-gradient-to-r !from-teal-400 !via-teal-500 !to-teal-600 hover:bg-gradient-to-br !font-bold !text-white !text-[16px]"
        >
          Lưu thay đổi
        </Button>
      </div>
    </form>
  );
};

export default AddFlim;
const InputS = styled(Input)`
  /* margin-top: 20px; */
  input {
    background-color: transparent !important;
    border: 1px solid black;
    color: black;
  }
`;
