import { toast } from "react-toastify";
import { styled } from "styled-components";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { Button, Input } from "..";
import { AccountSchema, AccountSchemaType } from "schema";
import { useAppDispatch } from "store";
import { addUserAdminThunk } from "store/quanLyNguoiDung/thunk";
import { useRef } from "react";

const AddUser = () => {
  const dispatch = useAppDispatch();
  const { register } = useForm<AccountSchemaType>({
    mode: "onSubmit",
    resolver: zodResolver(AccountSchema),
  });
  interface FormValues {
    [key: string]: string;
  }

  const formRef = useRef(null);
  const onSubmit = () => {
    if (formRef.current) {
      const formData = new FormData(formRef.current);
      const values: FormValues = {};

      formData.forEach((value, name) => {
        values[name] = value.toString();
      });

      dispatch(addUserAdminThunk(values))
        .unwrap()
        .then(() => {
          toast.success("Thêm người dùng Thành công");
        });
    }
  };
  return (
    <form
      className="px-10"
      onSubmit={(e) => {
        e.preventDefault();
        onSubmit();
      }}
      ref={formRef}
    >
      <InputS label="Tài khoản" name="taiKhoan" register={register} />
      <InputS label="Mật khẩu" name="matKhau" register={register} />
      <InputS label="Họ và tên" name="hoTen" register={register} />
      <InputS label="Số điện thoại" name="soDT" register={register} />
      <InputS label="Email" name="email" register={register} />
      <InputS
        label="Mã nhóm"
        name="maNhom"
        register={register}
        // disabled
        value="GP03"
      />
      <InputS
        label="Mã Người dùng"
        name="maLoaiNguoiDung"
        register={register}
      />
      <div className="text-right ">
        <Button
          htmlType="submit"
          className="mt-[20px] mb-[20px] w-[200px] !h-[50px] !bg-gradient-to-r !from-teal-400 !via-teal-500 !to-teal-600 hover:bg-gradient-to-br !font-bold !text-white !text-[16px]"
        >
          Thêm người dùng
        </Button>
      </div>
    </form>
  );
};

export default AddUser;
const InputS = styled(Input)`
  /* margin-top: 20px; */
  input {
    background-color: transparent !important;
    border: 1px solid black;
    color: black;
  }
`;
