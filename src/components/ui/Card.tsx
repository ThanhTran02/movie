import { useNavigate } from "react-router-dom";
import { Movie } from "types";

export const Card = ({ movie, path }: { movie: Movie; path: string }) => {
  const navigate = useNavigate();

  return (
    <div>
      <div className="flex max-w-sm w-full h-[470px] bg-white shadow-md rounded-lg overflow-hidden mx-auto">
        <div
          className="overflow-hidden w-full rounded-xl relative transform hover:-translate-y-2 transition ease-in-out duration-500 shadow-lg hover:shadow-2xl movie-item text-white movie-card"
          data-movie-id="438631"
        >
          <div className="absolute inset-0 z-10 transition duration-300 ease-in-out bg-gradient-to-t from-black via-gray-900 to-transparent"></div>
          <div
            className="relative cursor-pointer group z-10 px-10 pt-10 space-y-6 movie_info"
            data-lity=""
          >
            <div className="poster__info align-self-end w-full">
              <div className="h-32"></div>
              <div className="space-y-6 detail_info">
                <div className="flex flex-col space-y-2 inner">
                  <a
                    className="relative flex items-center w-min flex-shrink-0 p-1 text-center text-white bg-red-500 rounded-full group-hover:bg-red-700"
                    data-unsp-sanitized="clean"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="w-10 h-10"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                    >
                      <path
                        fillRule="evenodd"
                        d="M10 18a8 8 0 1 0 0-16 8 8 0 0 0 0 16zM9.555 7.168A1 1 0 0 0 8 8v4a1 1 0 0 0 1.555.832l3-2a1 1 0 0 0 0-1.664l-3-2z"
                        clipRule="evenodd"
                      ></path>
                    </svg>
                    <div className="absolute transition opacity-0 duration-500 ease-in-out transform group-hover:opacity-100 group-hover:translate-x-16 text-xl font-bold text-white group-hover:pr-2">
                      Trailer
                    </div>
                  </a>
                  <h3
                    className="text-2xl font-bold text-white h-20"
                    data-unsp-sanitized="clean"
                  >
                    {movie.tenPhim}
                  </h3>
                </div>
                <div className="flex flex-col overview h-20 w-full">
                  <div className="flex flex-col"></div>
                  <div className="text-xs text-gray-400 mb-2">Overview:</div>
                  <p className="text-xs text-gray-100 mb-6">
                    {movie.moTa.substring(0, 70)}
                  </p>
                </div>
              </div>
            </div>
          </div>
          <img
            className="absolute inset-0 transform w-full -translate-y-4 "
            src={movie.hinhAnh}
          />
          <div className="poster__footer flex flex-row relative pb-10 space-x-4 z-10 w-full cursor-pointer">
            <div
              className="flex items-center py-2 px-4 rounded-full mx-auto text-white bg-red-500 hover:bg-red-700"
              data-unsp-sanitized="clean"
              onClick={() => navigate(path)}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="w-6 h-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"
                ></path>
              </svg>
              <div className="text-sm text-white ml-2 ">Đặt vé</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
