import { Input as InputA, InputProps as InputPropsA } from "antd";
import { SearchProps } from "antd/es/input";
import React from "react";
type InputObject = {
  (props: InputPropsA): JSX.Element;
  Search: React.FC<SearchProps>;
};
export const InputAnt: InputObject = (props: InputPropsA) => {
  return <InputA {...props}></InputA>;
};
InputAnt.Search = InputA.Search;
export default InputAnt;
