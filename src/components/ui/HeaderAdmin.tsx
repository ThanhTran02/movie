import { useNavigate } from "react-router-dom";
import { AiFillHome } from "react-icons/ai";
import { useSelector } from "react-redux";

import { InputAnt, Modal, Popover } from ".";
import { RootState, useAppDispatch } from "store";
import { getMovieListAdminThunk } from "store/quanLyPhim/thunk";
import { SearchProps } from "antd/es/input";
import { getUserListThunkAdmin } from "store/quanLyNguoiDung/thunk";
import AddUser from "./AdminAdd/AddUser";
import { quanLyNguoiDungActions } from "store/quanLyNguoiDung/slice";

type HeaderAdminProps = {
  h2Content: string;
  buttonContent: string;
  inputPlaceholder: string;
  typeSearch?: string;
};

export const HeaderAdmin = ({
  h2Content,
  buttonContent,
  inputPlaceholder,
  typeSearch,
}: HeaderAdminProps) => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const { isModel } = useSelector((state: RootState) => state.quanLyNguoiDung);
  const showModal = () => {
    dispatch(quanLyNguoiDungActions.changeModel());
  };

  const handleCancel = () => {
    dispatch(quanLyNguoiDungActions.changeModel());
  };

  const onSearch: SearchProps["onSearch"] = (value: string) => {
    typeSearch == "user"
      ? dispatch(getUserListThunkAdmin(value))
      : dispatch(getMovieListAdminThunk(value));
  };

  return (
    <div className="items-start justify-start flex flex-col gap-2">
      <div className="flex justify-between w-full pr-10 items-center">
        <h2 className="font-medium text-3xl text-gray-500">{h2Content}</h2>
        <Popover
          content={
            <div>
              <h2 className="font-semibold p-2">Quản trị viên</h2>
              <hr />
              <div
                className="p-2 hover:bg-gray-500 hover:text-white cursor-pointer rounded-md"
                onClick={() => navigate("/")}
              >
                Quay về trang chính
              </div>
            </div>
          }
        >
          <AiFillHome className="w-10 h-10" />
        </Popover>
      </div>
      <button
        type="button"
        className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center mr-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
        onClick={showModal}
      >
        <svg
          className="w-3.5 h-3.5 mr-2"
          aria-hidden="true"
          xmlns="http://www.w3.org/2000/svg"
          fill="currentColor"
          viewBox="0 0 18 21"
        >
          <path d="M15 12a1 1 0 0 0 .962-.726l2-7A1 1 0 0 0 17 3H3.77L3.175.745A1 1 0 0 0 2.208 0H1a1 1 0 0 0 0 2h.438l.6 2.255v.019l2 7 .746 2.986A3 3 0 1 0 9 17a2.966 2.966 0 0 0-.184-1h2.368c-.118.32-.18.659-.184 1a3 3 0 1 0 3-3H6.78l-.5-2H15Z" />
        </svg>
        {buttonContent}
      </button>
      <InputAnt.Search
        placeholder={inputPlaceholder}
        allowClear
        enterButton="Search"
        size="large"
        onSearch={onSearch}
      />
      <Modal
        maskStyle={{ background: `rgba(0, 0, 0, 0.2)` }}
        width={1000}
        open={isModel}
        footer
        onCancel={handleCancel}
      >
        {typeSearch == "user" && <AddUser />}
      </Modal>
    </div>
  );
};

export default HeaderAdmin;
