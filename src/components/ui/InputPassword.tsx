import { useState } from "react";
import { UseFormRegister } from "react-hook-form";
import { FaEye, FaEyeSlash } from "react-icons/fa";
type InputPasswordProps = {
  register?: UseFormRegister<any>;
  error?: string;
  name?: string;
  placeholder?: string;
};

export const InputPassword = ({
  name,
  placeholder,
  error,
  register,
}: InputPasswordProps) => {
  const [showPassword, setShowPassword] = useState(false);

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };
  return (
    <div className="">
      <input
        type={showPassword ? "text" : "password"}
        placeholder={placeholder}
        className="outline-none  block w-full p-4 text-gray-900 placeholder-red-600 border border-red-600 rounded-lg bg-gray-50  "
        {...register?.(name || "")}
      />
      <div className="relative  bottom-[36px] left-[310px] transform -translate-y-[2px] bg-transparent border-none cursor-pointer">
        <button type="button" className="" onClick={togglePasswordVisibility}>
          <div className="text-xl text-gray-500 block">
            {showPassword ? <FaEyeSlash /> : <FaEye />}
          </div>
        </button>
      </div>
      {error && <p className="text-red-500 mt-2">{error}</p>}
    </div>
  );
};

export default InputPassword;
