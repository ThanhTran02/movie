import { Tabs } from "antd";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { RootState, useAppDispatch } from "store";
import {
  getCineplexThunk,
  getShowtimesCineplexThunk,
} from "store/quanLyRap/thunk";
import CardCinema from "./CardCinema";

export const CinemaTab = () => {
  const { cineplexList, movieList } = useSelector(
    (state: RootState) => state.quanLiRap
  );

  const { chooseCinema } = useSelector(
    (state: RootState) => state.quanLyNguoiDung
  );

  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(getCineplexThunk(chooseCinema));
    dispatch(getShowtimesCineplexThunk(chooseCinema));
  }, [chooseCinema]);
  return (
    <div className="h-[500px]">
      <Tabs
        tabPosition="left"
        className="h-full"
        tabBarGutter={-5}
        items={
          cineplexList &&
          cineplexList.map((cineplex, i) => {
            const id = String(i + 1);
            return {
              label: (
                <div className="w-72 text-start flex flex-col px-1" key={id}>
                  <p className="text-xl font-semibold">{cineplex.tenCumRap}</p>
                  <p className="break-all ">{cineplex.diaChi}</p>
                </div>
              ),
              key: id,
              children: <CardCinema movieList={movieList} />,
            };
          })
        }
      />
    </div>
  );
};

export default CinemaTab;
