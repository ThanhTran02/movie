import { PATH } from "constant";
import { generatePath, useNavigate } from "react-router-dom";

const CardCinema = ({ movieList }: { movieList: [] }) => {
  let path = "";
  const navigate = useNavigate();
  return (
    <div className="overflow-y-scroll h-[500px] pb-10 ">
      {movieList.map((movie: any, index: number) => (
        <div
          className=" bg-gray-100 flex flex-col justify-center  w-full"
          key={index}
        >
          <div className="py-3 w-full sm:mx-auto">
            <div className="bg-white shadow-lg border-gray-100	 border  p-2 flex  space-x-8">
              <div className="h-28 w-40">
                <img
                  className="rounded-xl shadow-lg h-full"
                  src={movie.hinhAnh}
                  alt={movie.tenPhim}
                />
              </div>
              <div className="flex flex-col w-full space-y-4">
                <div className="flex justify-between flex-col items-start">
                  <h2 className="text-xl font-bold">{movie.tenPhim}</h2>
                  <span className="font-bold">Lịch chiếu:</span>
                  <div className="grid grid-cols-3 gap-5">
                    {movie.lstLichChieuTheoPhim?.map(
                      (item: any, index: number) => {
                        if (index < 3)
                          return (
                            <div key={index}>
                              <button
                                type="button"
                                className="font-semibold"
                                onClick={() => {
                                  path = generatePath(PATH.purchase, {
                                    maLichChieu: item.maLichChieu,
                                  });
                                  navigate(path);
                                }}
                              >
                                <div className="flex gap-2 ">
                                  <span>Ngày chiếu: </span>
                                  <p>
                                    {item.ngayChieuGioChieu.substring(0, 10)}
                                  </p>
                                </div>
                                <div className="flex gap-2 ">
                                  <span>Giờ chiếu: </span>
                                  <p>{item.ngayChieuGioChieu.substr(11, 10)}</p>
                                </div>
                              </button>
                            </div>
                          );
                        else return <></>;
                      }
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default CardCinema;
