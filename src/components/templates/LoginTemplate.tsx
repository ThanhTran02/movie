import { zodResolver } from "@hookform/resolvers/zod";
import { SubmitHandler, useForm } from "react-hook-form";
import { RxAvatar } from "react-icons/rx";

import Input from "components/ui/Input";
import { LoginSchemaType, LoginSchema } from "schema";
import { useAppDispatch } from "store";
import { loginThunk } from "store/quanLyNguoiDung/thunk";
import { useNavigate } from "react-router-dom";
import { PATH } from "constant";
import { InputPassword } from "components/ui";
import { toast } from "react-toastify";

export const LoginTemplate = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<LoginSchemaType>({
    mode: "onChange",
    resolver: zodResolver(LoginSchema),
  });

  const onSubmit: SubmitHandler<LoginSchemaType> = async (value) => {
    dispatch(loginThunk(value))
      .unwrap()
      .then(() => {
        navigate("/");
      })
      .catch((error) => {
        toast.error(error.response.data.content);
      });
  };

  return (
    <form className="pt-5 h-full" onSubmit={handleSubmit(onSubmit)}>
      <div className="flex flex-col items-center">
        <RxAvatar className="h-10 w-10 text-red-500" />
        <h2 className="text-gray-700 text-2xl font-bold">Đăng nhập </h2>
      </div>

      <div className="mt-8 text-start">
        <Input
          register={register}
          name="taiKhoan"
          error={errors?.taiKhoan?.message}
          placeholder="Tài khoản"
        />
      </div>
      <div className="mt-8 text-start">
        <InputPassword
          register={register}
          name="matKhau"
          error={errors?.matKhau?.message}
          placeholder="Mật khẩu"
        />
      </div>
      <div className="mt-8 ">
        <div className="flex items-start mb-6">
          <div className="flex items-center h-5 ">
            <input
              type="checkbox"
              value=""
              className="w-4 h-4 border border-gray-300 rounded bg-gray-50 accent-red-600  "
            />
          </div>
          <label className="ml-2  font-medium text-gray-900 ">
            Nhớ tài khoản
          </label>
        </div>
        <button className="text-white w-full bg-red-500 font-500 rounded-lg text-20 px-5 py-[16px]">
          Đăng Nhập
        </button>
        <p className="mt-8 text-gray-900">
          Chưa có tài khoản?{" "}
          <span
            className="text-blue-500 cursor-pointer"
            onClick={() => {
              navigate(PATH.register);
            }}
          >
            Đăng ký
          </span>
        </p>
      </div>
    </form>
  );
};
export default LoginTemplate;
