import { Tabs } from "components/ui";
import AccountInfoTab from "./AccountInfoTab";
import { AccountBooking } from ".";

export const AccountTemplate = () => {
  return (
    <div className="w-full mt-20">
      <Tabs
        tabPosition="left"
        className="h-full"
        tabBarGutter={-5}
        items={[
          {
            label: (
              <div className="w-[200px]  hover:bg-gray-400 hover:text-white rounded-lg transition-all py-4 text-black ">
                <p className="text-xl text-left object-contain font-semibold px-4">
                  Thông tin tài khoản
                </p>
              </div>
            ),
            key: "accountInfo",
            children: <AccountInfoTab />,
          },
          {
            label: (
              <div className="w-[200px]  hover:bg-gray-400 hover:text-white rounded-lg transition-all py-4 text-black">
                <p className="text-xl text-left object-contain font-semibold px-4">
                  Thông tin đặt vé
                </p>
              </div>
            ),
            key: "ticketInfo",
            children: <AccountBooking />,
          },
        ]}
      />
    </div>
  );
};

export default AccountTemplate;
