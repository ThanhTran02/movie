import { zodResolver } from "@hookform/resolvers/zod";
import { Button, Input } from "components/ui";
import { useAuth } from "hooks";
import { useEffect } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { AccountSchema, AccountSchemaType } from "schema";
import { RootState, useAppDispatch } from "store";
import { updateUserThunk } from "store/quanLyNguoiDung/thunk";
import { styled } from "styled-components";

const AccountInfoTab = () => {
  const { user } = useAuth();
  const dispatch = useAppDispatch();
  const {
    reset,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<AccountSchemaType>({
    mode: "onChange",
    resolver: zodResolver(AccountSchema),
  });
  const onSubmit: SubmitHandler<AccountSchemaType> = (value) => {
    dispatch(updateUserThunk(value))
      .unwrap()
      .then(() => toast.success("Cập nhật thông tin tài khoản thành công"));
  };

  const { isUpdatingUser } = useSelector(
    (state: RootState) => state.quanLyNguoiDung
  );
  useEffect(() => {
    reset({ ...user, soDt: user?.soDT });
  }, [user, reset]);
  return (
    <form className="px-10" onSubmit={handleSubmit(onSubmit)}>
      <InputS
        label="Tài khoản"
        name="taiKhoan"
        register={register}
        error={errors?.taiKhoan?.message}
      />
      <InputS
        label="Họ và tên"
        name="hoTen"
        register={register}
        error={errors?.hoTen?.message}
      />
      <InputS
        label="Số điện thoại"
        name="soDt"
        register={register}
        error={errors?.soDt?.message}
      />
      <InputS
        label="Email"
        name="email"
        register={register}
        error={errors?.email?.message}
      />
      <InputS label="Mã nhóm" name="maNhom" register={register} />
      <InputS
        disabled={true}
        label="Mã Người dùng"
        name="maLoaiNguoiDung"
        register={register}
      />
      <div className="text-right ">
        <Button
          loading={isUpdatingUser}
          htmlType="submit"
          className="mt-[20px] mb-[20px] w-[200px] !h-[50px] !bg-gradient-to-r !from-teal-400 !via-teal-500 !to-teal-600 hover:bg-gradient-to-br !font-bold !text-white !text-[16px]"
        >
          Lưu thay đổi
        </Button>
      </div>
    </form>
  );
};

export default AccountInfoTab;

const InputS = styled(Input)`
  margin-top: 20px;
  input {
    background-color: transparent !important;
    border: 1px solid black;
    color: black;
  }
`;
