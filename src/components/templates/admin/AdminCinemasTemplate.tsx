import { HeaderAdmin, TableCinema } from "components/ui";

export const AdminCinemasTemplate = () => {
  return (
    <div>
      <div className="p-4 sm:ml-64 ">
        <div className="p-4 border-2 border-gray-200 border-dashed rounded-lg dark:border-gray-700 ">
          <div className="relative overflow-x-auto shadow-md sm:rounded-lg w-full ">
            <HeaderAdmin
              h2Content="Quản lý Rạp chiếu"
              buttonContent="Thêm Rạp chiếu"
              inputPlaceholder="Nhập mã Rạp chiếu "
            />
            <TableCinema />
          </div>
        </div>
      </div>
    </div>
  );
};

export default AdminCinemasTemplate;
