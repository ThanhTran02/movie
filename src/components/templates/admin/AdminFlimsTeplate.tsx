import { HeaderAdmin, TableFlims } from "components/ui";

export const AdminFlimsTemplate = () => {
  return (
    <div>
      <div className="p-4 sm:ml-64 ">
        <div className="p-4 border-2 border-gray-200 border-dashed rounded-lg dark:border-gray-700 ">
          <div className="relative overflow-x-auto shadow-md sm:rounded-lg w-full ">
            <HeaderAdmin
              h2Content="Quản lý Phim"
              buttonContent="Thêm Phim"
              inputPlaceholder="Nhập mã phim"
            />
            <TableFlims />
          </div>
        </div>
      </div>
    </div>
  );
};

export default AdminFlimsTemplate;
