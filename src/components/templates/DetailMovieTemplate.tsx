import { Footer, Modal, Navbar } from "components/ui";
import { PATH } from "constant";
import { useState, useRef, useEffect } from "react";
import { useSelector } from "react-redux";
import { generatePath, useNavigate, useParams } from "react-router-dom";
import { RootState, useAppDispatch } from "store";
import { getMoveDetailThunk } from "store/quanLyPhim/thunk";
import { getInfoLichChieuPhimThunk } from "store/quanLyRap/thunk";
import styled from "styled-components";
export const DetailMovieTemplate = () => {
  const params = useParams();
  const { movieId } = params;

  let path = "";

  const navigate = useNavigate();
  const videoRef = useRef<HTMLVideoElement | null>(null);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const { movie } = useSelector((state: RootState) => state.quanLiPhim);
  const { maLichChieu } = useSelector((state: RootState) => state.quanLiRap);

  const dispatch = useAppDispatch();

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    if (videoRef.current) {
      videoRef.current.pause();
    }
  };

  useEffect(() => {
    dispatch(getMoveDetailThunk(movieId));
    dispatch(getInfoLichChieuPhimThunk(movieId));
  }, []);

  return (
    <Container>
      <Navbar />
      <div>
        <div className="min-h-screen grid place-items-center font-mono bg-gray-900">
          <div className=" rounded-md bg-gray-800 shadow-lg">
            <div className="md:flex px-4 leading-none max-w-4xl">
              <div className="flex-none ">
                <img
                  src={movie?.hinhAnh}
                  alt="pic"
                  className="h-72 w-56 rounded-md  transform translate-y-4 border-4 border-gray-300 shadow-lg"
                />
              </div>

              <div className="flex-col text-gray-300">
                <p className="pt-4 text-2xl font-bold">{movie?.tenPhim}</p>
                <hr className="hr-text" data-content="" />
                <p className="hidden md:block px-4 my-4 text-sm text-left">
                  <span>Mô tả: </span>
                  {movie?.moTa}
                </p>

                <p className="flex text-md px-4 my-2">
                  Rating: 9.0/10
                  <span className="font-bold px-2">|</span>
                  Mood: Dark
                </p>

                <div className="text-xs">
                  <button
                    type="button"
                    className="border border-gray-400 text-gray-400 rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-gray-900 focus:outline-none focus:shadow-outline"
                    onClick={showModal}
                  >
                    TRAILER
                  </button>

                  <button
                    type="button"
                    className="border border-gray-400 text-gray-400 rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-gray-900 focus:outline-none focus:shadow-outline"
                  >
                    IMDB
                  </button>

                  <button
                    type="button"
                    className="border border-gray-400 text-gray-400 rounded-md px-4 py-2 m-2 transition duration-500 ease select-none hover:bg-gray-900 focus:outline-none focus:shadow-outline"
                    onClick={() => {
                      path = generatePath(PATH.purchase, {
                        maLichChieu: maLichChieu,
                      });
                      navigate(path);
                    }}
                  >
                    Đặt vé
                  </button>
                </div>
              </div>
            </div>
            <div className="flex justify-between items-center px-4 mb-4 w-full"></div>
          </div>
        </div>
      </div>
      <Modal
        open={isModalOpen}
        footer
        onCancel={handleCancel}
        className="!w-3/4"
      >
        <video className="aspect-video w-full p-3" controls ref={videoRef}>
          <source src="/trailer/trailer.mp4" type="video/mp4" />
        </video>
      </Modal>
      <Footer />
    </Container>
  );
};

export default DetailMovieTemplate;
const Container = styled.div`
  body {
    text-align: center;
  }

  .hr-text {
    line-height: 1em;
    position: relative;
    outline: 0;
    border: 0;
    color: black;
    text-align: center;
    height: 1.5em;
    opacity: 0.5;
  }
  .hr-text:before {
    content: "";
    background: linear-gradient(to right, transparent, #818078, transparent);
    position: absolute;
    left: 0;
    top: 50%;
    width: 100%;
    height: 1px;
  }
  .hr-text:after {
    content: attr(data-content);
    position: relative;
    display: inline-block;
    color: black;
    padding: 0 0.5em;
    line-height: 1.5em;
    color: #818078;
    background-color: #fcfcfa;
  }
`;
