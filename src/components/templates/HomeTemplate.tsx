import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { styled } from "styled-components";

import { RootState, useAppDispatch } from "store";
import { Card, Carousel, CinemaTab, Pagination, Tabs } from "components/ui";
import { getMoveBannerThunk, getMoveListThunk } from "store/quanLyPhim/thunk";
import { quanLiPhimActions } from "store/quanLyPhim/slice";
import { generatePath } from "react-router-dom";
import { PATH } from "constant";
import { getCinemaListThunk } from "store/quanLyRap/thunk";
import { quanLyNguoiDungActions } from "store/quanLyNguoiDung/slice";

export const HomeTemplate: React.FC = () => {
  const dispatch = useAppDispatch();

  const { movieBanner, movieList, currentPage } = useSelector(
    (state: RootState) => state.quanLiPhim
  );
  const { cinemaList, cineplexList } = useSelector(
    (state: RootState) => state.quanLiRap
  );

  const handlePageChange = (page: number) => {
    dispatch(quanLiPhimActions.setCurrentPage(page));
  };

  const handleTabClick = (key: any) => {
    dispatch(quanLyNguoiDungActions.changeCinema(key));
    // Xử lý logic tại đây
  };
  let path = "";
  useEffect(() => {
    dispatch(getMoveBannerThunk());
    dispatch(getMoveListThunk(currentPage));
    dispatch(getCinemaListThunk());
  }, [currentPage, cineplexList]);
  return (
    <Container className="w-full">
      <Carousel autoplay className="text-black">
        {movieBanner.map((movie) => (
          <div key={movie.maPhim} className="w-full h-[600px]">
            <img
              src={movie.hinhAnh}
              alt={movie.maPhim}
              className="object-cover w-full h-full"
            />
          </div>
        ))}
      </Carousel>
      <h2
        id="lichChieu"
        className="font-serif text-transparent py-6 bg-clip-text text-4xl bg-gradient-to-r to-orange-700 from-red-800"
      >
        MOVIE SELECTION
      </h2>
      <div className="grid grid-cols-4 gap-10">
        {movieList.map((item) => {
          path = generatePath(PATH.detail, { movieId: item.maPhim });
          return <Card movie={item} key={item.maPhim} path={path} />;
        })}
      </div>
      <Pagination
        defaultCurrent={1}
        onChange={handlePageChange}
        total={50}
        className=" !my-2"
      />
      <div id="cumRap" className="my-20 h-[500px]">
        <Tabs
          tabPosition="left"
          className="h-full "
          tabBarGutter={-5}
          type="card"
          onTabClick={handleTabClick}
          items={
            cinemaList &&
            cinemaList.map((cinema) => {
              return {
                label: (
                  <div>
                    <img
                      src={cinema.logo}
                      alt="rap"
                      className="w-16 h-16 object-contain "
                    />
                  </div>
                ),

                key: cinema.maHeThongRap,
                children: <CinemaTab />,
              };
            })
          }
        />
      </div>
    </Container>
  );
};

export default HomeTemplate;
const Container = styled.div`
  width: var(--max-width);
  button {
    /* background: yellow !important; */
    height: 5px !important;
  }
`;
