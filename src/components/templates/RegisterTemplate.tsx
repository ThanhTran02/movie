import { zodResolver } from "@hookform/resolvers/zod";
import { AiFillLock } from "react-icons/ai";

import { Input, InputPassword } from "components/ui";
import { SubmitHandler, useForm } from "react-hook-form";
import { RegisterSchema, RegisterSchemaType } from "schema";
import { quanLyNguoiDungServices } from "services";
import { PATH } from "constant";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { isAxiosError } from "axios";

export const RegisterTemplate = () => {
  const navigate = useNavigate();
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<RegisterSchemaType>({
    mode: "onChange",
    resolver: zodResolver(RegisterSchema),
  });
  const onSubmit: SubmitHandler<RegisterSchemaType> = async (value) => {
    try {
      await quanLyNguoiDungServices.register(value);
      navigate(PATH.login);
      toast.success("Đăng ký tài khoản thành công");
    } catch (error) {
      if (isAxiosError<{ content: string }>(error)) {
        toast.error(error.response?.data.content);
      }
    }
  };
  return (
    <form className="pt-10" onSubmit={handleSubmit(onSubmit)}>
      <div className="flex flex-col items-center">
        <AiFillLock className="h-10 w-10 text-red-500" />
        <h2 className="text-gray-700 text-2xl font-bold">Đăng ký </h2>
      </div>
      <div className="mt-6 text-start">
        <Input
          register={register}
          name="hoTen"
          error={errors?.hoTen?.message}
          placeholder="Họ tên"
        />
      </div>
      <div className="mt-6 text-start">
        <Input
          register={register}
          name="taiKhoan"
          error={errors?.taiKhoan?.message}
          placeholder="Tài khoản"
        />
      </div>
      <div className="mt-6 text-start">
        <InputPassword
          register={register}
          name="matKhau"
          error={errors?.matKhau?.message}
          placeholder="Mật khẩu"
        />
      </div>
      <div className="mt-6 text-start">
        <Input
          register={register}
          name="email"
          error={errors?.email?.message}
          placeholder="Email"
        />
      </div>
      <div className="mt-6 text-start">
        <Input
          register={register}
          name="soDt"
          error={errors?.soDt?.message}
          placeholder="Số điện thoại"
        />
      </div>
      <div className="mt-6 text-start">
        <Input
          register={register}
          name="maNhom"
          error={errors?.maNhom?.message}
          placeholder="Mã nhóm"
        />
      </div>
      <div className="mt-6 ">
        <button className="text-white w-full bg-red-500 font-500 rounded-lg text-20 px-5 py-[16px]">
          Đăng Ký
        </button>
        <p className="mt-6 text-gray-900">
          Bạn đã có tài khoản?{" "}
          <span
            className="text-blue-500 cursor-pointer"
            onClick={() => navigate(PATH.login)}
          >
            Đăng nhập
          </span>
        </p>
      </div>
    </form>
  );
};

export default RegisterTemplate;
