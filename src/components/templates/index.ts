export * from "./LoginTemplate";
export * from "./RegisterTemplate";
export * from "./HomeTemplate";

export * from "./DetailMovieTemplate";

export * from "./Account";
export * from "./admin";
export * from "./PurchaseTemplate";
