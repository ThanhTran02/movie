import axios, { CreateAxiosDefaults, AxiosRequestHeaders } from "axios";

const TOKEN_CYBERSOFT =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0NiIsIkhldEhhblN0cmluZyI6IjIyLzAxLzIwMjUiLCJIZXRIYW5UaW1lIjoiMTcwNTg4MTYwMDAwMCIsIm5iZiI6MTY3ODI5NDgwMCwiZXhwIjoxNzA2MDI5MjAwfQ.bL_fJw3nksOSSoB7Y4LIbWofMMKQmlgDHF8C8L-9edY";


export const apiInstance = (config?: CreateAxiosDefaults) => {
  const api = axios.create(config);
  api.interceptors.request.use((config) => {
    return {
      ...config,
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: "Bearer" + " " + localStorage.getItem("accessToken"),
      } as unknown as AxiosRequestHeaders,
    };
  });
  return api;
};
