export const PATH = {
  login: "/login",
  register: "/register",
  home: "/home",
  account: "/account",
  admin: "/admin",
  users: "/users",
  cinemas: "/cinemas",
  films: "/films",
  detail: "/movie/:movieId",
  purchase: "/purchase/:maLichChieu",
};
