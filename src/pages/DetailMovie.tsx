import { DetailMovieTemplate } from "components/templates";

const DetailMovie = () => {
  return <DetailMovieTemplate />;
};

export default DetailMovie;
