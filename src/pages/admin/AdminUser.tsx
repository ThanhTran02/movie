import { AdminUserTemplate } from "components/templates";

export const AdminUser = () => {
  return <AdminUserTemplate />;
};

export default AdminUser;
