import { z } from "zod";

export const MovieSchema = z.object({
  maPhim: z.number().or(z.string()),
  tenPhim: z.string().nonempty("Vui lòng nhập tên phim"),
  biDanh: z.string().nonempty("Vui lòng nhập bí danh"),
  trailer: z.string(),
  hinhAnh: z.string(),
  moTa: z.string().nonempty("Vui lòng nhập mô tả"),
  maNhom: z.string().nonempty("Vui lòng nhập tài khoản"),
  ngayKhoiChieu: z.string().nonempty("Vui lòng nhập ngày khởi chiếu"),
  danhGia: z.number(),
  hot: z.boolean(),
  dangChieu: z.boolean(),
  sapChieu: z.boolean(),
});
export type MovieSchemaType = z.infer<typeof MovieSchema>;
