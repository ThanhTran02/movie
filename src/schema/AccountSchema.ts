import { z } from "zod";

export const AccountSchema = z.object({
  taiKhoan: z.string().nonempty("Vui lòng nhập tài khoản"),
  email: z
    .string()
    .nonempty("Vui lòng nhập Email")
    .email("Vui lòng nhập đúng Email"),
  soDt: z.string().nonempty("Vui lòng nhập số điện thoại"),
  maNhom: z.string().nonempty("Vui lòng nhập mã nhóm"),
  hoTen: z.string().nonempty("Vui lòng nhập họ tên"),
  maLoaiNguoiDung: z.string().nonempty(""),
});
export type AccountSchemaType = z.infer<typeof AccountSchema>;
