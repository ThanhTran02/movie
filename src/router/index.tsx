import { AdminLayout, AuthLayout, MainLayout } from "components/layouts";
import Purchase from "components/templates/PurchaseTemplate";
import { PATH } from "constant";
import { Account, AdminUser, Home, Login, Register } from "pages";
import Detail from "pages/DetailMovie";
import AdminCinema from "pages/admin/AdminCinema";
import AdminFilms from "pages/admin/AdminFilms";

import { RouteObject } from "react-router-dom";

export const router: RouteObject[] = [
  {
    path: "/",
    element: <MainLayout />,
    children: [
      {
        index: true,
        element: <Home />,
      },
      {
        path: PATH.account,
        element: <Account />,
      },
      {
        path: PATH.purchase,
        element: <Purchase />,
      },
    ],
  },
  {
    path: PATH.detail,
    element: <Detail />,
  },

  {
    element: <AuthLayout />,
    children: [
      {
        path: PATH.login,
        element: <Login />,
      },
      {
        path: PATH.register,
        element: <Register />,
      },
    ],
  },
  {
    element: <AdminLayout />,
    children: [
      {
        path: PATH.users,
        element: <AdminUser />,
      },
      {
        path: PATH.films,
        element: <AdminFilms />,
      },
      {
        path: PATH.cinemas,
        element: <AdminCinema />,
      },
    ],
  },
];
