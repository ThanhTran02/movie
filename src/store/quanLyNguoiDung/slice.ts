import { createSlice } from "@reduxjs/toolkit";
import {
  addUserAdminThunk,
  getUserListThunkAdmin,
  getUserThunk,
  loginThunk,
  updateUserThunk,
} from "./thunk";
import { User, UserInfo } from "types";

type QuanLyNguoiDungInitailState = {
  user?: UserInfo | User;
  isUpdatingUser?: boolean;

  isModel?: boolean;
  // admin
  userList?: any;
  chooseCinema: any;
};
const initialState: QuanLyNguoiDungInitailState = {
  user: undefined,
  isUpdatingUser: false,
  userList: [],
  chooseCinema: "BHDStar",
  isModel: false,
};
export const quanLiNguoiDungSlice = createSlice({
  name: "quanLyNguoiDung",
  initialState,
  reducers: {
    logOut: (state) => {
      localStorage.removeItem("accessToken");
      state.user = undefined;
    },
    changeCinema: (state, { payload }) => {
      state.chooseCinema = payload;
    },
    changeModel: (state) => {
      state.isModel = !state.isModel;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loginThunk.fulfilled, (state, { payload }) => {
        state.user = payload;
        if (payload) localStorage.setItem("accessToken", payload.accessToken);
      })
      .addCase(getUserThunk.fulfilled, (state, { payload }) => {
        if (payload) {
          state.user = payload;
        }
      })
      .addCase(updateUserThunk.pending, (state) => {
        state.isUpdatingUser = true;
      })
      .addCase(updateUserThunk.fulfilled, (state) => {
        state.isUpdatingUser = false;
      })
      .addCase(updateUserThunk.rejected, (state) => {
        state.isUpdatingUser = false;
      })

      // admin
      .addCase(getUserListThunkAdmin.fulfilled, (state, { payload }) => {
        state.userList = payload;
      })
      .addCase(addUserAdminThunk.fulfilled, (state) => {
        state.isModel = !state.isModel;
      });
  },
});
export const {
  reducer: quanLiNguoiDungReducer,
  actions: quanLyNguoiDungActions,
} = quanLiNguoiDungSlice;
