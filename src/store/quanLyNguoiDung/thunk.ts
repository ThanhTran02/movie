import { createAsyncThunk } from "@reduxjs/toolkit";
import { AccountSchemaType, LoginSchemaType } from "schema";
import { quanLyNguoiDungServices } from "services";

export const loginThunk = createAsyncThunk(
  "quanLyNguoiDung/loginThunk",
  async (payload: LoginSchemaType, { rejectWithValue }) => {
    try {
      const data = await quanLyNguoiDungServices.login(payload);

      return data.data.content;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
export const getUserThunk = createAsyncThunk(
  "quanLyNguoiDung/getUser",
  async (_, { rejectWithValue }) => {
    try {
      const accessToken = localStorage.getItem("accessToken");
      if (accessToken) {
        const data = await quanLyNguoiDungServices.getUser();
        return data.data.content;
      }

      return undefined;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
export const updateUserThunk = createAsyncThunk(
  "quanLyNguoiDung/updateUser",
  async (payload: AccountSchemaType, { rejectWithValue, dispatch }) => {
    try {
      await quanLyNguoiDungServices.updateUser(payload);
      await new Promise((resolve) => setTimeout(resolve, 500));
      dispatch(getUserThunk());
    } catch (err) {
      rejectWithValue(err);
    }
  }
);

// admin
export const getUserListThunkAdmin = createAsyncThunk(
  "QuanLyNguoiDung/ThongTinTaiKhoan",
  async (payload: string = "", { rejectWithValue }) => {
    try {
      let data;
      if (payload != "") {
        data = await quanLyNguoiDungServices.getSearchUserListAdmin(payload);
      } else {
        data = await quanLyNguoiDungServices.getUserListAdmin();
      }
      return data.data.content;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
export const deleteUserThunkAdmin = createAsyncThunk(
  "QuanLyNguoiDung/XoaNguoiDung",
  async (payload: string, { rejectWithValue }) => {
    try {
      const query = `?TaiKhoan=${payload}`;
      const data = await quanLyNguoiDungServices.deleteUserAdmin(query);

      return data.data.content;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
export const updateUserAdminThunk = createAsyncThunk(
  "quanLyNguoiDung/CapNhatThongTinNguoiDung",
  async (payload: any, { rejectWithValue }) => {
    try {
      const data = await quanLyNguoiDungServices.updateUserAdmin(payload);
      await new Promise((resolve) => setTimeout(resolve, 500));
      return data.data.content;
    } catch (err) {
      rejectWithValue(err);
    }
  }
);
export const addUserAdminThunk = createAsyncThunk(
  "quanLyNguoiDung/ThemNguoiDung",
  async (payload: any, { rejectWithValue, dispatch }) => {
    try {
      await quanLyNguoiDungServices.addUserAdmin(payload);
      await new Promise((resolve) => setTimeout(resolve, 500));
      dispatch(getUserListThunkAdmin(""));
    } catch (err) {
      rejectWithValue(err);
    }
  }
);
