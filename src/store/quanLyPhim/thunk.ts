import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyPhimService } from "services";
import { quanLyNguoiDungActions } from "store/quanLyNguoiDung/slice";

export const getMoveBannerThunk = createAsyncThunk(
  "quanLyPhim/getMoveBannerThunk",
  async (_, { rejectWithValue }) => {
    try {
      const data = await quanLyPhimService.getMovieBanner();
      return data.data.content;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const getMoveListThunk = createAsyncThunk(
  "quanLyPhim/LayDanhSachPhimPhanTrang",
  async (page: number, { rejectWithValue }) => {
    try {
      const query = `?maNhom=GP01&soTrang=${page}&soPhanTuTrenTrang=10`;
      const data = await quanLyPhimService.getMovieList(query);
      return data.data.content.items;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
export const getMoveDetailThunk = createAsyncThunk(
  "quanLyPhim/LayThongTinPhim",
  async (payload: string | undefined, { rejectWithValue }) => {
    try {
      const query = `?MaPhim=${payload}`;
      const data = await quanLyPhimService.getMovieDetail(query);
      return data.data.content;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

// admin

export const getMovieListAdminThunk = createAsyncThunk(
  "quanLyPhim/laydanhsachphim",
  async (payload: string = "", { rejectWithValue }) => {
    try {
      let data;
      if (payload != "") {
        data = await quanLyPhimService.getSearchMovieListAdmin(payload);
      } else {
        data = await quanLyPhimService.getMovieListAdmin();
      }
      return data.data.content;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
export const deleteMovieThunkAdmin = createAsyncThunk(
  "QuanLyPhim/XoaPhim",
  async (payload: number, { rejectWithValue }) => {
    try {
      const query = `?MaPhim=${payload}`;
      await new Promise((resolve) => setTimeout(resolve, 1000));
      const data = await quanLyPhimService.deleteMovieAdmin(query);
      return data.data.content;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
export const editMovieThunkAdmin = createAsyncThunk(
  "QuanLyPhim/CapNhatPhimUpload",
  async (payload: FormData, { rejectWithValue }) => {
    try {
      const data = await quanLyPhimService.editMovieAdmin(payload);

      return data.data.content;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
export const addMovieThunkAdmin = createAsyncThunk(
  "QuanLyPhim/ThemPhimUploadHinh",
  async (payload: any, { rejectWithValue, dispatch }) => {
    try {
      await quanLyPhimService.addMovieAdmin(payload);
      dispatch(getMovieListAdminThunk(""));
      dispatch(quanLyNguoiDungActions.changeModel());
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
