import { createSlice } from "@reduxjs/toolkit";
import { Movie, MovieBaner } from "types";
import {
  getMoveBannerThunk,
  getMoveDetailThunk,
  getMoveListThunk,
  getMovieListAdminThunk,
} from "./thunk";

type QuanLiPhimInitialState = {
  movieBanner: MovieBaner[];
  isFetchingMovieList: boolean;
  movieList: Movie[];
  movieListAdmin: Movie[];
  currentPage: number;
  movie: Movie | undefined;
};

const initialState: QuanLiPhimInitialState = {
  movieBanner: [],
  isFetchingMovieList: false,
  movieList: [],
  movieListAdmin: [],
  currentPage: 1,
  movie: undefined,
};
const quanLyPhimSlice = createSlice({
  name: "quanLyPhim",
  initialState,
  reducers: {
    setCurrentPage: (state, action) => {
      state.currentPage = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getMoveBannerThunk.pending, (state) => {
        state.isFetchingMovieList = true;
      })
      .addCase(getMoveBannerThunk.fulfilled, (state, { payload }) => {
        state.movieBanner = payload;
        state.isFetchingMovieList = false;
      })
      .addCase(getMoveBannerThunk.rejected, (state) => {
        state.isFetchingMovieList = false;
      })
      //   getMovieList
      .addCase(getMoveListThunk.pending, (state) => {
        state.isFetchingMovieList = true;
      })
      .addCase(getMoveListThunk.fulfilled, (state, { payload }) => {
        state.movieList = payload;
        state.isFetchingMovieList = false;
      })
      .addCase(getMoveListThunk.rejected, (state) => {
        state.isFetchingMovieList = false;
      })
      .addCase(getMoveDetailThunk.fulfilled, (state, { payload }) => {
        state.movie = payload;
      })

      // getMovieDetail
      .addCase(getMovieListAdminThunk.fulfilled, (state, { payload }) => {
        state.movieListAdmin = payload;
      });
  },
});

export const { reducer: quanLiPhimReducer, actions: quanLiPhimActions } =
  quanLyPhimSlice;
