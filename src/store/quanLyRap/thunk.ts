import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyRapService } from "services";

export const getCinemaListThunk = createAsyncThunk(
  "quanLyRap/getCinemaListThunk ",
  async (_, { rejectWithValue }) => {
    try {
      const data = await quanLyRapService.getCinema();
      return data.data.content;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const getCineplexThunk = createAsyncThunk(
  "quanLyRap/LayThongTinCumRapTheoHeThong ",
  async (payload: string, { rejectWithValue }) => {
    try {
      const query = `?maHeThongRap=${payload}`;
      const data = await quanLyRapService.getCineplex(query);
      return data.data.content;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
export const getShowtimesCineplexThunk = createAsyncThunk(
  "quanLyRap/LayThongTinLichChieuHeThongRap",
  async (payload: string, { rejectWithValue }) => {
    try {
      const query = `?maHeThongRap=${payload}&maNhom=GP01`;
      const data = await quanLyRapService.getShowtimesCineplex(query);

      return data.data.content[0].lstCumRap[0].danhSachPhim;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
export const getInfoLichChieuPhimThunk = createAsyncThunk(
  "quanLyRap/LayThongTinLichChieuPhim",
  async (payload: string | undefined, { rejectWithValue }) => {
    try {
      const query = `?maPhim=${payload}`;
      const data = await quanLyRapService.getInfoLichChieuPhim(query);

      return data.data.content.heThongRapChieu[0].cumRapChieu[0]
        .lichChieuPhim[0].maLichChieu;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);
