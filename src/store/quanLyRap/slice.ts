import { createSlice } from "@reduxjs/toolkit";
import { Cinema, Cineplexs } from "types";
import {
  getCinemaListThunk,
  getCineplexThunk,
  getInfoLichChieuPhimThunk,
  getShowtimesCineplexThunk,
} from "./thunk";

type QuanLyRapInitailState = {
  cinemaList?: Cinema[];
  cineplexList: Cineplexs[];
  movieList: [];
  maLichChieu: number;
};
const initialState: QuanLyRapInitailState = {
  cinemaList: [],
  cineplexList: [],
  movieList: [],
  maLichChieu: 41124,
};
export const quanLiRapSlice = createSlice({
  name: "quanLyRap",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getCinemaListThunk.fulfilled, (state, { payload }) => {
        state.cinemaList = payload;
      })
      .addCase(getShowtimesCineplexThunk.fulfilled, (state, { payload }) => {
        state.movieList = payload;
      })
      .addCase(getCineplexThunk.fulfilled, (state, { payload }) => {
        state.cineplexList = payload;
      })
      .addCase(getInfoLichChieuPhimThunk.fulfilled, (state, { payload }) => {
        state.maLichChieu = payload;
      });
  },
});
export const { reducer: quanLiRapReducer, actions: quanLyRapActions } =
  quanLiRapSlice;
