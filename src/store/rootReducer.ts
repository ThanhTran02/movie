import { combineReducers } from "@reduxjs/toolkit";
import { quanLiNguoiDungReducer } from "./quanLyNguoiDung/slice";
import { quanLiPhimReducer } from "./quanLyPhim/slice";
import { quanLiRapReducer } from "./quanLyRap/slice";
import { quanLyDatVeReducer } from "./quanLyDatVe/slice";
export const rootReducer = combineReducers({
  quanLyNguoiDung: quanLiNguoiDungReducer,
  quanLiPhim: quanLiPhimReducer,
  quanLiRap: quanLiRapReducer,
  quanLyDatVe: quanLyDatVeReducer,
});
