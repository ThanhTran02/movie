import { createSlice } from "@reduxjs/toolkit";
import { getDanhSachPhongVeThunk } from "./thunk";
import { Chair } from "types";
type QuanLyDatVeInitailState = {
  movieInfo: any;
  chairList: Chair[];
  chooseSeats: Chair[];
  chooseSeated: Chair[];
};
const initialState: QuanLyDatVeInitailState = {
  chairList: [],
  movieInfo: undefined,
  chooseSeats: [],
  chooseSeated: [],
};
export const quanLiDatVeSlice = createSlice({
  name: "quanLyPhongVe",
  initialState,
  reducers: {
    setSeat: (state, { payload }) => {
      const index = state.chooseSeats.findIndex(
        (seat) => seat.stt === payload.stt
      );
      if (index !== -1) state.chooseSeats.splice(index, 1);
      else state.chooseSeats.push(payload);
    },
    setSeatBooked: (state) => {
      state.chooseSeated = [...state.chooseSeated, ...state.chooseSeats];
      state.chooseSeats = [];

      //lưu vao local
      if (localStorage.getItem("localSeated")) {
        localStorage.removeItem("localSeated");
      }
      const localSeated = JSON.stringify(state.chooseSeated);
      localStorage.setItem("localSeated", localSeated);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getDanhSachPhongVeThunk.fulfilled, (state, { payload }) => {
      state.movieInfo = payload.thongTinPhim;
      //lưu vao local
      if (localStorage.getItem("movieChoosed")) {
        localStorage.removeItem("movieChoosed");
      }
      const localMovie = JSON.stringify(state.movieInfo);
      localStorage.setItem("movieChoosed", localMovie);

      state.chairList = payload.danhSachGhe;
    });
  },
});
export const { reducer: quanLyDatVeReducer, actions: quanLyDatVeActions } =
  quanLiDatVeSlice;
